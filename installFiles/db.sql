CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` smallint(6) DEFAULT 0,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `text` text NOT NULL,
  `admin_edit` varchar(32) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

INSERT INTO `tasks` (`id`, `status`, `name`, `email`, `text`, `admin_edit`) VALUES
(1, 0, 'test', 'test@test.com', 'qwe qwe qwe ', '0'),
(2, 0, 'test', 'test@test.com', 'test job', '0'),
(3, 0, 'test', 'test@test.rt', '&lt;script&gt;alert(\'test\');&lt;/script&gt;', '0'),
(4, 0, 'test', 'test@test.rt', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', '0');

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `pass` varchar(36) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

INSERT INTO `users` (`id`, `name`, `pass`) VALUES
(1, 'admin', 'aed6048c407f90e3f2a3b09835616cef');
