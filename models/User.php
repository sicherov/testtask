<?php

class User
{
    const SALT = "df9yhj!(";

    public static function register($name, $password)
    {

        $db = Db::getConnection();

        $sql = 'INSERT INTO users (name, pass) '
            . 'VALUES (:name, :pass)';

        $password = self::getMD5PassWithSalt($password);

        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':pass', $password, PDO::PARAM_STR);

        return $result->execute();
    }

    private static function getMD5PassWithSalt($pass)
    {
        return md5($pass.self::SALT);
    }

    public static function checkUserData($name, $password)
    {
        $db = Db::getConnection();

        $sql = 'SELECT * FROM users WHERE name = :name AND pass = :pass';
        $password = self::getMD5PassWithSalt($password);
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_INT);
        $result->bindParam(':pass', $password, PDO::PARAM_INT);
        $result->execute();

        $user = $result->fetch();
        if ($user) {
            return $user['id'];
        }

        return false;
    }

    public static function auth($userId)
    {
        $_SESSION['user'] = $userId;
    }

    public static function checkLogged()
    {
        if (isset($_SESSION['user'])) {
            return $_SESSION['user'];
        }
        return false;
    }

    public static function isGuest()
    {
        if (isset($_SESSION['user'])) {
            return false;
        }
        return true;
    }

    public static function checkName($name)
    {
        if (strlen($name) >= 3 && strlen($name) <= 128) {
            return true;
        }
        return false;
    }

    public static function checkPassword($password)
    {
        if (strlen($password) >= 3) {
            return true;
        }
        return false;
    }

    public static function checkUserExists($name)
    {

        $db = Db::getConnection();

        $sql = 'SELECT COUNT(*) FROM users WHERE name = :name';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->execute();

        if ($result->fetchColumn())
            return true;
        return false;
    }

    public static function getUserById($id)
    {
        if ($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM users WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);

            $result->execute();


            return $result->fetch();
        }
    }

}
