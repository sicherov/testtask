<?php

class Task
{

    const SHOW_BY_DEFAULT = 3;

    static $sortFields = array("id","name","email","status");


    public static function getTasks($count = self::SHOW_BY_DEFAULT, $page = 1)
    {
        $count = intval($count);
        $page = intval($page);
        $offset = ($page-1) * $count;

        $db = Db::getConnection();
        $taskList = array();

        $result = $db->query('SELECT * FROM tasks '
                . 'ORDER BY ' . $_SESSION["sortField"] . " " . $_SESSION["sortOrder"]
                . ' LIMIT ' . $count
                . ' OFFSET '. $offset);

        while ($row = $result->fetch()) {
            $taskList[] = $row;
        }

        return $taskList;
    }


    public static function getTaskById($id)
    {
        $id = intval($id);

        if ($id) {
            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM tasks WHERE id=' . $id);

            return $result->fetch();
        }
    }

    public static function getSortOrder($field)
    {
        if($_SESSION["sortField"]==$field){
            if($_SESSION["sortOrder"]==Db::$sortOrder[0]){
                return Db::$sortOrder[1];
            }else{
                return Db::$sortOrder[0];
            }
        }else{
            return Db::$sortOrder[0];
        }
    }
    public static function getTotalTasks()
    {
        $db = Db::getConnection();

        $result = $db->query('SELECT count(id) AS count FROM tasks');
        $row = $result->fetch();

        return $row['count'];
    }

    public static function checkName($name)
    {
        if (strlen($name) >= 3 && strlen($name) <= 128) {
            return true;
        }
        return false;
    }
    public static function checkText($text)
    {
        if (strlen($text) >= 1) {
            return true;
        }
        return false;
    }
    public static function checkEmail($email)
    {
        if (filter_var($email, FILTER_VALIDATE_EMAIL) && strlen($email) <= 128) {
            return true;
        }
        return false;
    }

    public static function add($name, $email, $text)
    {

        $db = Db::getConnection();

        $sql = 'INSERT INTO tasks (name, email, text) '
            . 'VALUES (:name, :email, :text)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);

        return $result->execute();
    }
    public static function edit($id,$text,$status,$adminEdit)
    {

        $db = Db::getConnection();

        $sql = "UPDATE tasks 
            SET  text=:text, status=:status, admin_edit=:admin_edit
            WHERE id = :id";
        $result = $db->prepare($sql);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        $result->bindParam(':status', $status, PDO::PARAM_STR);
        $result->bindParam(':admin_edit', $adminEdit, PDO::PARAM_STR);
        $result->bindParam(':id', $id, PDO::PARAM_STR);

        return $result->execute();
    }
}
