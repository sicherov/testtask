<?php

class TaskController
{

    public function actionIndex($page = 1,$sortField="id",$sortOrder="asc")
    {
        $TITLE = "Список задач";

        if(isset($_GET["sortField"]) || isset($_GET["sortOrder"])){
            if(in_array($_GET["sortField"],Task::$sortFields)){
                $_SESSION["sortField"] = $_GET["sortField"];
            }
            if(in_array($_GET["sortOrder"],Db::$sortOrder)){
                $_SESSION["sortOrder"] = $_GET["sortOrder"];
            }
        }
        if(!isset($_SESSION["sortField"])){
            $_SESSION["sortField"] = Task::$sortFields[0];
        }
        if(!isset($_SESSION["sortOrder"])){
            $_SESSION["sortOrder"] = Db::$sortOrder[0];
        }
        $latestTasks = array();
        $latestTasks = Task::getTasks(Task::SHOW_BY_DEFAULT,$page);

        $total = Task::getTotalTasks();

        $pagination = new Pagination($total, $page, Task::SHOW_BY_DEFAULT, 'p');

        require_once(ROOT . '/views/task/index.php');

        return true;
    }

    public function actionAdd()
    {
        $TITLE = "Добавить задачу";
        $name = "";
        $email = "";
        $text = "";
        $result = false;
        if (isset($_POST['submit'])) {
            $name = htmlentities($_POST['name']);
            $email = $_POST['email'];
            $text = htmlentities($_POST['text']);

            $errors = false;

            if (!Task::checkName($name)) {
                $errors[] = 'Имя не должно быть короче 3 и не длиннее 128 символов';
            }

            if (!Task::checkEmail($email)) {
                $errors[] = 'Неправильный email';
            }
            if (!Task::checkText($text)) {
                $errors[] = 'Нет текста задачи';
            }
            if ($errors == false) {
                $result = Task::add($name, $email, $text);
            }

        }

        require_once(ROOT . '/views/task/add.php');

        return true;
    }

}
