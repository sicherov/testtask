<?php

class UserController
{

    public function actionRegister()
    {

        $name = '';
        $password = '';
        $result = false;
        $TITLE = "Регистрация";
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче 2-х символов';
            }


            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 6-ти символов';
            }

            if (!User::checkUserExists($name)) {
                $errors[] = 'Такой пользователь уже существует';
            }

            if ($errors == false) {
                $result = User::register($name, $password);
            }

        }

        require_once(ROOT . '/views/user/register.php');

        return true;
    }

    public function actionLogin()
    {
        $name = '';
        $password = '';
        $TITLE = "Авторизация";
        if (isset($_POST['submit'])) {
            $name = $_POST['name'];
            $password = $_POST['password'];

            $errors = false;

            if (!User::checkName($name)) {
                $errors[] = 'Имя не должно быть короче 3 и не длиннее 128 символов';
            }

            if (!User::checkPassword($password)) {
                $errors[] = 'Пароль не должен быть короче 3-ти символов';
            }

            $userId = User::checkUserData($name, $password);

            if ($userId == false) {
                $errors[] = 'Неправильные данные для входа на сайт';
            } else {
                User::auth($userId);
                Router::redirect("/admin/");
            }

        }

        require_once(ROOT . '/views/user/login.php');

        return true;
    }

    public function actionLogout()
    {
        session_start();
        unset($_SESSION["user"]);
        Router::redirect("/");
    }

}
