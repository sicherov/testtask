<?php

class AdminController
{

    public function actionIndex($page = 1,$sortField="id",$sortOrder="asc")
    {
        $userId = User::checkLogged();
        if(!$userId){
            Router::redirect("/login");
        }

        $TITLE = "Админка";

        if(isset($_GET["sortField"]) || isset($_GET["sortOrder"])){
            if(in_array($_GET["sortField"],Task::$sortFields)){
                $_SESSION["sortField"] = $_GET["sortField"];
            }
            if(in_array($_GET["sortOrder"],Db::$sortOrder)){
                $_SESSION["sortOrder"] = $_GET["sortOrder"];
            }
        }
        if(!isset($_SESSION["sortField"])){
            $_SESSION["sortField"] = Task::$sortFields[0];
        }
        if(!isset($_SESSION["sortOrder"])){
            $_SESSION["sortOrder"] = Db::$sortOrder[0];
        }
        $latestTasks = array();
        $latestTasks = Task::getTasks(Task::SHOW_BY_DEFAULT,$page);

        $total = Task::getTotalTasks();

        $pagination = new Pagination($total, $page, Task::SHOW_BY_DEFAULT, 'p');

        require_once(ROOT . '/views/admin/index.php');

        return true;
    }

    public function actionEdit($id)
    {
        if(!User::checkLogged()) {
            Router::redirect("/login");
        }

        $task = Task::getTaskById($id);
        if(!$task) {
            Router::redirect("/admin");
        }

        $TITLE = "Изменить задачу";
        $name = $task["name"];
        $email = $task["email"];
        $text = $task["text"];
        $status = $task["status"];
        $adminEdit = $task["admin_edit"];
        $result = false;
        if (isset($_POST['submit']) && $task) {
            $id=$task["id"];

            $text = htmlentities($_POST['text']);
            $status = isset($_POST['status'])?1:0;
            $errors = false;

            if($text != $task["text"]){
                $adminEdit = date("Y-m-d H:i:s");
            }

            if (!Task::checkText($text)) {
                $errors[] = 'Нет текста задачи';
            }


            if ($errors == false) {
                $result = Task::edit($id, $text,$status,$adminEdit);
            }

        }

        require_once(ROOT . '/views/admin/edit.php');

        return true;
    }

}
