<?php

class SiteController
{

    public static function action404()
    {
        $TITLE = "404";
        require_once(ROOT . '/views/site/404.php');

        return true;
    }

    public function actionInstall()
    {
        $TITLE = "Установка";

        $result = Db::install();

        require_once(ROOT . '/views/site/install.php');

        return true;
    }
}

