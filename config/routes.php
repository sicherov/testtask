<?php

return array(

    'task/([0-9]+)' => 'product/view/$1', // детальная страница задачи

    'add' => 'task/add', //добавление задачи

    'login' => 'user/login', //авторизация
    'register' => 'user/register', //регистрация
    'logout' => 'user/logout', //выход

    'admin/edit/([0-9]+)' => 'admin/edit/$1',//админка редактирование
    'admin/p([0-9]+)' => 'admin/index/$1',//админка постраничка
    'admin' => 'admin/index',//админка

    'install' => 'site/install',//установка базы

    'p([0-9]+)' => 'task/index/$1', // страницы списка
    '' => 'task/index', // главная, список

);
