<?php include ROOT . '/views/layouts/header.php'; ?>
<section class="main">


        <form class="justify-content-center text-center" action="#" method="post">

            <div class="form-group">
                <label for="inputName">Имя</label>
                <input type="text" class="form-control" name="name" id="inputName" disabled value="<?=$name?>" >
            </div>
            <div class="form-group">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" name="email" id="inputEmail" disabled value="<?=$email?>">
            </div>
            <div class="form-group">
                <label for="inputText">Задача</label>
                <textarea class="form-control" id="inputText" name="text" rows="3"><?=$text?></textarea>
            </div>
            <div class="form-group">
                <label for="inputStatus">Выполенно</label>
                <input type="checkbox" class="form-control" name="status" id="inputStatus" <?=$status?"checked":""?>>
            </div>
            <?if($adminEdit){?>
                <div class="form-group">
                    Отредактировано администратором <div><?=$adminEdit?></div>
                </div>
            <?}?>
            <input  type="submit" name="submit" class="btn btn-primary">
            <? if (isset($errors) && is_array($errors)){ ?>
                <ul>
                    <? foreach ($errors as $error){ ?>
                        <li><?php echo $error; ?></li>
                    <? } ?>
                </ul>
            <? } ?>
            <? if ($result){ ?>
                <div class="success-add">
                    <p>Задача обновлена!</p>
                </div>
            <? } ?>
        </form>

</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
