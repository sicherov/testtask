<?php include ROOT . '/views/layouts/header.php'; ?>
<section class="main">
<table class="table table-hover table-dark">
    <thead>
    <tr>
        <th scope="col" class="table-id-field">
            <a href="/<?=Router::getURI()?>?sortField=id&sortOrder=<?=Task::getSortOrder("id")?>">
                Id
                <?=Db::getSortOrderIcon(Task::getSortOrder("id"))?>
            </a>
        </th>
        <th scope="col" class="table-name-field">
            <a href="/<?=Router::getURI()?>?sortField=name&sortOrder=<?=Task::getSortOrder("name")?>">
                Имя
                <?=Db::getSortOrderIcon(Task::getSortOrder("name"))?>
            </a>
        </th>
        <th scope="col" class="table-email-field">
            <a href="/<?=Router::getURI()?>?sortField=email&sortOrder=<?=Task::getSortOrder("email")?>">
                Email
                <?=Db::getSortOrderIcon(Task::getSortOrder("email"))?>
            </a>
        </th>
        <th scope="col">Задача</th>
        <th scope="col" class="table-status-field">
            <a href="/<?=Router::getURI()?>?sortField=status&sortOrder=<?=Task::getSortOrder("status")?>">
                Статус
                <?=Db::getSortOrderIcon(Task::getSortOrder("status"))?>
            </a>
        </th>
        <th scope="col">Изменено администратором</th>
        <th scope="col">Управление</th>
    </tr>
    </thead>
    <tbody>
    <?foreach ($latestTasks as $taskItem){?>
        <tr>
            <td><?=$taskItem["id"]?></td>
            <td><?=$taskItem["name"]?></td>
            <td><?=$taskItem["email"]?></td>
            <td><?=$taskItem["text"]?></td>
            <td><?if($taskItem["status"]==1){?>
                    <div class="green-round"></div>
                <?}else{?>
                    <div class="red-round"></div>
                <?}?>
            </td>
            <td><?if($taskItem["admin_edit"]!=0){?>
                    <div class="green-round"></div>
                <?}else{?>
                    <div class="red-round"></div>
                <?}?>
            </td>
            <td><a href="/admin/edit/<?=$taskItem["id"]?>">Изменить</a></td>
        </tr>

    <?}?>
    </tbody>
</table>

</section>
<section class="pagination-section">
    <?php echo $pagination->get(); ?>
</section>
<?php include ROOT . '/views/layouts/footer.php'; ?>
