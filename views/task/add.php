<?php include ROOT . '/views/layouts/header.php'; ?>
<section class="main">

    <?php if ($result){ ?>
        <div class="success-add">
            <p>Задача добавленна!</p>
            <a href="/">В список задач</a>
        </div>
    <?php }else{ ?>
        <form class="justify-content-center  text-center" action="#" method="post">

            <div class="form-group">
                <label for="inputName">Имя</label>
                <input type="text" class="form-control" name="name" id="inputName" value="<?=$name?>" >
            </div>
            <div class="form-group">
                <label for="inputEmail">Email</label>
                <input type="email" class="form-control" name="email" id="inputEmail"  value="<?=$email?>">
            </div>
            <div class="form-group">
                <label for="inputText">Задача</label>
                <textarea class="form-control" id="inputText" name="text" rows="3"><?=$text?></textarea>
            </div>
            <input  type="submit" name="submit" class="btn btn-primary">
            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li><?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </form>
    <?php } ?>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
