<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <?php if ($result){ ?>
        <p>Успешная регистрация</p>
    <?}else{ ?>

        <form class="justify-content-center" action="#" method="post">

            <div class="form-group">
                <label for="inputName">Имя</label>
                <input type="text" class="form-control" name="name" id="inputName" value="<?=$name?>" >
            </div>
            <div class="form-group">
                <label for="inputPassword">Пароль</label>
                <input type="password" class="form-control" id="inputPassword" name="password" value="<?=$password?>">
            </div>
            <input  type="submit" name="submit" class="btn btn-primary">
            <?php if (isset($errors) && is_array($errors)): ?>
                <ul>
                    <?php foreach ($errors as $error): ?>
                        <li><?php echo $error; ?></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </form>

    <?}?>

</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
